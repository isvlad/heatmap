const bundleOutputDir = './dist';

const path = require('path');
const webpack = require('webpack');

const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const TerserPlugin = require('terser-webpack-plugin');

module.exports = env => {
  const isDevBuild = !(env && env.prod);
  const isAnalyze = env && env.analyze;
  const isMinimize = env && env.min || !isDevBuild

  console.log('wp base config env == ', env);

  return {
    mode: isDevBuild ? 'development' : 'production',
    entry: {
      tracker1: './src/front/entries/tracker1',
      tracker2: './src/front/entries/tracker2',
      heatmap:'./src/front/entries/heatMap',
    },

    resolve: {
      extensions: ['.js'],
    },
    output: {
      path: path.join(__dirname, 'wwwroot', 'dist'),
      //path: path.join(__dirname, bundleOutputDir),
      publicPath: '/dist/',
      filename: 'js/[name].js'
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /[\\/]node_modules[\\/]/,
          use: { loader: 'babel-loader' }
        },
      ]
    },
    plugins: [].concat(isDevBuild ? [
      new webpack.SourceMapDevToolPlugin({
        filename: '[file].map',
        moduleFilenameTemplate: path.relative(bundleOutputDir, '[resourcePath]')
      })
    ] : [], isAnalyze ? [new BundleAnalyzerPlugin()] : []),

    optimization: {
      runtimeChunk: {
        name: entrypoint => entrypoint.name
      },
      // runtimeChunk:'multiple',
      minimize: isMinimize,
      minimizer: [
        new TerserPlugin({
          cache: true,
          parallel: true,
          sourceMap: true,
          terserOptions: {
            ecma: 5,
            warnings: false,
            parse: {},
            compress: { warnings: false, drop_console: true },
            ie8: false,
            safari10: true
          }
        }),
      ],
    }
  }

};
