module.exports = function (api) {
  return {
    "presets": [
      [
        "@babel/preset-env",
        {
          "useBuiltIns": "usage",
          "modules": false,
          "corejs": {
            "version": 3,
            "proposals": true
          },
          targets: {
            node: 'current',
          },
        }
      ]
    ],
    "plugins": [].concat(api.env('test') ? ["transform-es2015-modules-commonjs"] : []),
    "comments": true
  }
}
