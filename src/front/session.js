// * или любой другой способ передать сессию
export function getSessionId() {
  return document.cookie.replace(/(?:(?:^|.*;\s*)MyTrackerApp\s*\=\s*([^;]*).*$)|^.*$/, "$1") || undefined
}
