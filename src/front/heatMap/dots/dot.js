import renderElement from '../../renderer/createElement';

export default class Dot {
  constructor(ev, parent, info, { color = '#123', size = '5px' }) {
    this.ev = ev;
    this.el =
      renderElement(['div', {
        style: {
          position: 'absolute',
          backgroundColor: color,
          width: size,
          height: size,
        },
        classes: ['heat_dot', ev.type, 'c']
      }])

    this.setCoords(this.getCoords());

    parent.appendChild(this.el);

    // * возможно перенести в хитмап с опредилением точек и тд (можно показывать точки по радиусу)
    this.el.addEventListener('mouseenter', () => info.show({ coord: this.getCoords(), dataset: [this.ev] }));
  }

  getCoords() { return this.ev.coordinates; }

  setCoords({ pageX, pageY }) {
    this.el.style.left = pageX + 'px';
    this.el.style.top = pageY + 'px';
  }

  setColor(color = '#123') { this.el.style.background = color; }

  __show() { this.el.style.display = 'block'; }
  __hide() { this.el.style.display = 'none'; }


  get show() { return this.el.style.display !== 'none'; }
  set show(val) { val ? this.__show() : this.__hide(); }

}