/** custom createElement */
import xcE from '../../renderer/createElement';

export default function createInfo(...args) {

  const $el = xcE(['div', {
    style: {
      position: 'absolute',
      background: '#fff9'
    }
  }])

  let hto = null;

  const show = ({ coord, dataset }) => {
    if (hto) { clearTimeout(hto); }

    $el.style.display = 'block';
    $el.style.left = coord.pageX + 'px';
    $el.style.top = coord.pageY + 'px';

    $el.innerHTML = '';

    $el.innerHTML = dataset.reduce((acc, cur) => acc + columnTable(cur), '')// renderDataSet(dataset);
  }

  const elWp = xcE(['div',
    {
      style: {
        position: 'absolute',
        zIndex: 7001,
        top: '0px'
      }
    },
    [$el]
  ])

  document.body.appendChild(elWp);

  const hide = () => hto = setTimeout(() => $el.style.display = 'none', 400);

  $el.addEventListener('mouseleave', () => hto = setTimeout(() => $el.style.display = 'none', 400));
  $el.addEventListener('mouseenter', () => hto && clearTimeout(hto));

  return { show, hide }
}

function columnTable(dataObj, columnCount = 2) {
  let ds = [...Object.entries(dataObj)]
  
  let res = '<table style="border:1px solid black; margin:5px;"><tbody>';
  let length = ds.length;
  for (let i = 0; i < Math.ceil(length / columnCount); i++) {
    res += '<tr>';
    [...Array(columnCount).keys()].forEach(() => {
      let tmp = ds.pop();
      if (tmp) {
        let [key, value] = tmp;

        if (typeof value === 'object') {
          value = columnTable(value)
        }
        res += `<td>${key}&nbsp;:&nbsp;${value}</td>`
      }
    })
    res += '</tr>'
  }

  return `${res}</tbody></table>`
}