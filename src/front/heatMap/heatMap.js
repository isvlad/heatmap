import Dot from './dots/dot';
import createInfo from './dots/info';
import createControlls from './controlls';

import renderElement from '../renderer/createElement';

import mapColor from './colorMapper'

/**
 * @typedef HeatMap
 * @type {Object}
 * @property {Function} addDot - add new dot.  
 *
 * @param {Array<Tracker>} trackers 
 *
 * @param {string} Tracker.el - query selector
 * @param {string} Tracker.trackerId - Tracker Id 
 *
 * @return {HeatMap}
 */
export default function createHeatMap(trackers) {

  trackers = trackers.map(tr => ({ ...tr, el: document.querySelector(tr.el) }))

  const dots = [];

  const heatmap = renderElement(['div', {
    style: {
      position: 'absolute',
      zIndex: 7000,
      top: '0px'
    }, classes: ['heat_map']
  }])
  document.body.appendChild(heatmap)

  const info = createInfo();
  const eventsTypes = ['click', 'mousedown', 'mouseup', 'touchstart', 'touchmove', 'touchend'];
  const controls = createControlls({ eventsTypes, trackers: trackers.map(x => x.trackerId) });

  const filters = {
    type: (ctx, triger) => triger === 'all' || ctx.type === triger,
    trackerId: (ctx, triger) => triger === 'all' || ctx.trackerId === triger,
    rangeLeft: (ctx, triger) => !(triger - 0) || ctx.timeStamp > (triger - 0),
    rangeRight: (ctx, triger) => !(triger - 0) || ctx.timeStamp < (triger - 0),
    sessionId: (ctx, triger) => !triger || ctx.sessionId === triger,
  }

  const calcFilter = () => Object.entries(controls.model).reduce((acc, [key, model]) => model.value && acc.concat([(ctx) => filters[key](ctx, model.value)]) || acc, [])

  let lastFilters = calcFilter();

  const updateFilters = () => {
    lastFilters = calcFilter();
    dots.forEach(dot => dot.show = lastFilters.every((cb) => cb(dot.ev)));
  }

  Object.values(controls.model).map((value) => value.onchange = updateFilters)

  const addDot = (ev) => {
    const display = lastFilters.every((cb) => cb(ev))
    const dot = new Dot(ev, heatmap, info, { color: mapColor(ev.type, eventsTypes) });
    dot.show = display;
    dots.push(dot);
  }

  return { addDot };
}