const colors = ['#BE0032', '#F3C300', '#875692', '#F38400', '#A1CAF1', '#C2B280', '#008856', '#E68FAC', '#0067A5', '#F99379', '#604E97', '#F6A600', '#B3446C', '#DCD300', '#882D17', '#8DB600', '#654522', '#848482', '#E25822', '#2B3D26', '#F2F3F4', '#222222']

export default function mapColor(type, types, defColor = '#123') {
    const index = types.indexOf(type);

    if (index === -1) {
        return defColor;
    }

    return colors[index]
}