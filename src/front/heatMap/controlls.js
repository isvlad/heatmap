

// create helper
import xcE from '../renderer/createElement';

const createCleanButton = (modelToClean, defVal) => xcE([
  'button', {
    attrs: [['type', 'number']],
    listners: [['click', () => modelToClean.value = defVal]]
  },
  [xcE('X')]
]);

const inputStyles = { width: '177px' }

const defSOption = 'all'

export default function createControlls({ eventsTypes, trackers }) {

  const model = {
    type: { value: defSOption },
    trackerId: { value: defSOption },
    rangeLeft: { value: '' },
    rangeRight: { value: '' },
    sessionId: { value: '' },
  }

  const elements = [];

  let typeEl = xcE(['div', {}, [
    xcE('Event type '),
    xcE(['select', {
      style: inputStyles,
      model: model.type,
    },
      [defSOption, ...eventsTypes].map(type => xcE(['option', { attrs: [['value', type]] }, [xcE(type)]]))]),
    createCleanButton(model.type, 'all')
  ]]);

  elements.push(typeEl)

  let reackerEl = xcE(['div', {}, [
    xcE('Tracker '),
    xcE(['select', {
      style: inputStyles,
      model: model.trackerId,
    },
      [defSOption, ...trackers].map(type => xcE(['option', { attrs: [['value', type]] }, [xcE(type)]]))]),
    createCleanButton(model.trackerId, 'all')
  ]]);

  elements.push(reackerEl)


  let sessionEl = xcE([
    'div',
    {},
    [
      document.createTextNode('Session Id '),
      xcE(['input', { style: inputStyles, model: model.sessionId }]),
      createCleanButton(model.sessionId, ''),
    ]
  ])

  elements.push(sessionEl);


  let timestampEl = xcE(['div', {}, [
    xcE(['h4', {
      style: { margin: 0, textAlign: 'center', }
    }, [xcE('Timestamp-range ')]]),

    xcE(['div', {}, [
      xcE('from '),
      xcE(['input', { style: inputStyles, attrs: [['type', 'number'], ['min', '0']], model: model.rangeLeft }]),
      createCleanButton(model.rangeLeft),
    ]]),

    xcE(['div', {}, [
      xcE('to '),
      xcE([['input'], { style: inputStyles, attrs: [['type', 'number'], ['min', '0']], model: model.rangeRight }]),
      createCleanButton(model.rangeRight),
    ]]),
  ]]);

  elements.push(timestampEl);

  const $el = xcE(['div', {
    style: {
      top: '0px', let: '0px',
      padding: '.7em',
      position: 'absolute', background: '#555',
      fontSize: '20px',
      textAlign: 'right',
    },
    classes: ['controlls_box'],
  }, elements]);

  document.body.appendChild($el);

  return { model }
}