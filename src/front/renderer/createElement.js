/** native document.createElement(elementToCreate) */
const ncE = function (elementToCreate) { return document.createElement(elementToCreate) };

export default function createElement(createElParams) {

  if (isPrimitive(createElParams)) { return document.createTextNode(createElParams) }

  const [tag, opts, childrens = []] = createElParams

  const $el = ncE(tag)


  childrens.forEach((child) => $el.appendChild(child))

  Object.entries(opts).forEach(([key, value]) => {
    switch (key) {
      case 'style': Object.entries(value).forEach(([sKey, sValue]) => aplyAttr($el[key], sKey, sValue)); break;
      case 'classes': value.forEach(staticClass => $el.classList.add(staticClass)); break;
      case 'model': bindModel($el, value); break;
      case 'listners': value.forEach(([on, cb]) => addEventListener($el, on, cb)); break;
      case 'attrs': value.forEach(([atr, value]) => aplyAttr($el, atr, value)); break;
    }
  })

  return $el
}

export function aplyAttr($el, atr, value) {
  $el[atr] = value;
}

export function bindModel($el, model) {
  let defaultVal = model.value;

  // Object.defineProperty(model, '__listeners', {  });
  let __listeners = [];
  Object.defineProperty(model, 'onchange', {

    set(value) {
      __listeners.push(value);
    },

  });

  Object.defineProperty(model, 'value', {
    get() { return $el.value },

    set(value) {
      $el.value = value;
      __listeners.forEach(cb => cb(value));
    }
  });

  model.value = defaultVal;



  addEventListener($el, 'input', (ev) => model.value = ev)
}

export function addEventListener($el, e, cb) {
  $el.addEventListener(e, (ev) => cb(ev.target.value, ev, ev.target))
}

export function isPrimitive(value) {
  return (
    typeof value === 'string' ||
    typeof value === 'number' ||
    typeof value === 'symbol' ||
    typeof value === 'boolean'
  )
}
