import createHeatMap from '../heatMap/heatMap'

const heatMap = createHeatMap([{ el: '#tracker1', trackerId: 'tracker 1' }, { el: '#tracker2', trackerId: 'tracker 2' }]);
window.heatMap = heatMap;

async function tick() {
    let arr = window.events || [];
    let event;
    while (event = arr.pop()) {

        if (event) {
            heatMap.addDot(event)
        }
    }
}
const timmer = setInterval(tick, 250)


