import {getSessionId} from '../session';
import createTrecker from '../tracker/tracker';


createTrecker({
  el: '#toTrack1',
  trackerId: 'tracker 1',
  sessionId: getSessionId()||'session1'
})
