import {getSessionId} from '../session';
import createTrecker from '../tracker/tracker';

createTrecker({
  el: '#toTrack2',
  trackerId: 'tracker 2',
  sessionId: getSessionId()||'session1'
})
