const MAX_TIME_OUT = 5000;
const BASE_TIME_OUT = 250;
const APM_TIME_OUT = 1.5;

const TRACK_EVENTS = ['click', 'mousedown', 'mouseup', 'touchstart', 'touchmove', 'touchend']

const GATE_URL = `/track`

import { forceTransform, serialize } from './utils';

/**
 * @param {object} opts cfg tracker
 * @param {string} opts.el - element query selector
 * @param {string} opts.trackerId - tracker unique name
 * @param {string} opts.sessionId - sessionId from server
 */
export default function createTrecker({ el, trackerId, sessionId, }) {
  const EL = document.querySelector(el);

  // ! demo
  window.events = window.events || []

  let
    qEvents = [],
    qCashe = false,
    timeOut = BASE_TIME_OUT;

  const startSendQ = async () => {
    if (qCashe) {
      timeOut = timeOut < MAX_TIME_OUT ? timeOut * APM_TIME_OUT : timeOut;
      return;
    }

    qCashe = true;
    let q;
    while (q = qEvents.pop()) {
      await new Promise((resolve, reject) =>
        setTimeout(() => send(q).then(resolve).catch(reject)
          , timeOut)
      )
    }
    qCashe = false;
  }

  const send = async (event) => {
    
    try {

      // let { ok, statusText } = await fetch(GATE_URL, { cache: "no-cache", method: "POST", body: JSON.stringify(event) })
      let { ok, statusText } = await fetch(`/`, { cache: "no-cache" })
      
      if (!ok) { throw new Error(statusText) }

      timeOut = BASE_TIME_OUT;
    } catch (error) {
      qEvents.push(event);
      startSendQ()
    }

    //! demo
    window.events = [...window.events, event];
  }

  const handler = (event) => {

    let { timeStamp, type, x, y, changedTouches, clientX, clientY,
      pageX, pageY,
      screenX, screenY } = event;
    let coordinates = {
      x, y, clientX, clientY,
      pageX, pageY,
      screenX, screenY
    };

    let res = {
      timeStamp, type, coordinates, screen: forceTransform(window.screen), trackerId, sessionId
    }

    if (changedTouches) {
      let touches = changedTouches && [...changedTouches].map(({
        clientX, clientY,
        pageX, pageY,
        screenX, screenY
      }) => ({
        coordinates: {
          clientX, clientY,
          pageX, pageY,
          screenX, screenY
        }
      }));

      touches.forEach(item => send({ ...res, ...item }))

    } else send(res)
  }

  TRACK_EVENTS.forEach(evName => EL.addEventListener(evName, handler, { passive: true }, true));
}
