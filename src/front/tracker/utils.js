export function forceTransform(obj, dep = 2) {
  let res = {}

  if (!obj) { return res; }

  let keys = Object.keys(Object.getPrototypeOf(obj))
  keys.forEach(key => {
    if (typeof obj[key] === 'object') {
      if (dep > 0) {
        let tmp = forceTransform(obj[key], dep - 1)
        if (Object.keys(tmp).length) {
          res[key] = tmp;
        }
      }

    } else {
      if (typeof obj[key] !== 'function') {
        res[key] = obj[key];
      }
    }
  })

  return res;
}

export function serialize(obj) {
  let res = '';
  let a = Object.entries(obj)
  return a.reduce((acc, [key, value]) => {
    if (typeof value === 'object') {
      value = JSON.stringify(value)
    }
    return `${acc}&${key}=${value}`
  }, '')
}