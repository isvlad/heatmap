function g() {
  __p && __p();
  var xhr = new XMLHttpRequest();
  xhr && (xhr.open("GET", f.toString(), !0),
    xhr.withCredentials = !0,
    xhr.onreadystatechange = function () {
      __p && __p();
      if (xhr.readyState === 4) {
        if (b("sdk.feature")("e2e_ping_tracking", !0)) {
          var f = {
            init: e,
            close: ES("Date", "now", !1),
            method: "cors"
          };
          b("Log").debug("e2e: %s", ES("JSON", "stringify", !1, f));
          b("sdk.Impressions").log(m, {
            payload: f
          })
        }
        if (xhr.status === 200) {
          B(c, xhr.status, (f = xhr.getResponseHeader("fb-s")) != null ? f : "unknown", (f = xhr.getResponseHeader("fb-ar")) != null ? f : "{}")
        } else
          C(c, xhr.status, a)
      }
    }
    ,
    xhr.send())
}

function h() {
  window.fetch(data, {
    referrer: "/",
    mode: "cors",
    credentials: "include"
  }).then(function (b) {
    if (b.status === 200) {
    } else {}
  })["catch"](function (b) {
    return C(c, 0, a)
  })
}
typeof window.fetch === "function" ? h() : g()